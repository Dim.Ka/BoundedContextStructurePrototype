﻿using Topshelf;

namespace SomeBoundedContext.Host
{
    internal sealed class Program
    {
        private static void Main()
        {
            HostFactory.Run(_ =>
            {
                _.Service<Service>(s =>
                {
                    s.ConstructUsing(name => new Service());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });

                _.RunAsLocalSystem();
                _.OnException(s => { });
            });
        }
    }
}