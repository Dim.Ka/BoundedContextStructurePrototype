﻿using SomeBoundedContext.Infrastructure.NServiceBus.SomeBusiness1_Area;

namespace SomeBoundedContext.Host
{
    internal sealed class Service
    {
        private SomeBoundedContextMicroservice _microservice;

        public void Start()
        {
            _microservice = new SomeBoundedContextMicroservice();
            _microservice.Build();

            // Just for test conteiner.
            var someBusiness1AreaBusHandler = _microservice.Container.GetInstance<SomeBusiness1AreaBusHandler>();
        }

        public void Stop()
        {
            if (_microservice != null)
            {
                _microservice.Stop();
                _microservice = null;
            }
        }
    }
}