﻿using SomeBoundedContext.Contracts.Commands;
using SomeBoundedContext.Infrastructure.DataBase;

namespace SomeBoundedContext.Application.SomeBusiness1_Area
{
    internal sealed class SomeBusiness1ApplicationService
    {
        // Stateless, ctor injection

        private readonly SomeImplemetationOfConcretReporitory _repository;
        
        // An application layer is a just manager for requests, queries. App layer haven't to know which technologies for storing we use (some of db, cloud store, file store)
        // DAO has to hide access to data for queries.
        public SomeBusiness1ApplicationService(SomeImplemetationOfConcretReporitory repository)
        {
            _repository = repository;
        }


        public void Handle(SomeCommand command)
        {
            // transaction for arr root by Id.

            var aggregateRoot = _repository.GetById(command.Data.Id);
            aggregateRoot.DoBusinessOperation(command.Data);
            _repository.SaveChanges();
        }
    }
}