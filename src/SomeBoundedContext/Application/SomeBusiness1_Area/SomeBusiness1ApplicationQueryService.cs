﻿using System;
using SomeBoundedContext.Contracts.Queries.SomeBusiness1Area;
using SomeBoundedContext.Infrastructure.DataBase;

namespace SomeBoundedContext.Application.SomeBusiness1_Area
{
    internal sealed class SomeBusiness1ApplicationQueryService : ISomeBusiness1AreaQuery
    {
        // Stateless, ctor injection

        private readonly SomeImplemetationOfConcretDAO _dao;

        // An application layer is a just manager for requests, queries. App layer haven't to know which technologies for storing we use (some of db, cloud store, file store)
        // DAO has to hide access to data for queries.
        public SomeBusiness1ApplicationQueryService(SomeImplemetationOfConcretDAO dao)
        {
            _dao = dao;
        }

        public string GetCityByName(string name)
        {
            // Same responsible for anticorruption layer
            if (name.Length > 100)
                throw new Exception();
            
            return _dao.GetCityByName(name);
        }

        public string[] GetNamesByCity(string city)
        {
            return _dao.GetNamesByCity(city);
        }

        public Guid[] GetSomeBusinessIdsByCity(string city)
        {
            var someBusinessIdsByCity = _dao.GetCityByName(city);

            // Can combination an information. Light business layer.

            return _dao.GetSomeBusinessIdsByCity(someBusinessIdsByCity);
        }
    }
}