﻿using SomeBoundedContext.Contracts.Dtos;

namespace SomeBoundedContext.Domain.SomeBusiness1_Area
{
    internal sealed class SomeBusiness1AggregateRoot /* : AggRoot. Inhereted from something in NuGet Shared lib */
    {
        // Just aggRoots have to be stored to DB


        public void DoBusinessOperation(SomeData commandData)
        {
            // DomainEvent inside aggRoot
        }
    }
}