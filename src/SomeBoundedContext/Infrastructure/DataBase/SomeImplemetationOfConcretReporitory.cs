﻿using System;
using SomeBoundedContext.Domain.SomeBusiness1_Area;

namespace SomeBoundedContext.Infrastructure.DataBase
{
    internal sealed class SomeImplemetationOfConcretReporitory /* : IRepository. Inhereted from something in NuGet Shared lib */
    {
        // If we use ORM. We have to hide accessing to data in DAO.

        // Just 2 methods. StateLess. Transient

        public SomeBusiness1AggregateRoot GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public void SaveChanges()
        {
        }
    }
}