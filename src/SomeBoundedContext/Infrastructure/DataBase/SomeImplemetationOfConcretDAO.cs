﻿using System;

namespace SomeBoundedContext.Infrastructure.DataBase
{
    internal sealed class SomeImplemetationOfConcretDAO
    {
        // Stateless, Transient

        // If we use ORM. We have to hide accessing to data in DAO.
        public string GetCityByName(string name)
        {
            throw new System.NotImplementedException();
        }

        public string[] GetNamesByCity(string city)
        {
            throw new System.NotImplementedException();
        }

        public Guid[] GetSomeBusinessIdsByCity(string someBusinessIdsByCity)
        {
            throw new NotImplementedException();
        }
    }
}