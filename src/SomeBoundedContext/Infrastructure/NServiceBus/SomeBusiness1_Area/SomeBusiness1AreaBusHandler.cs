﻿using System;
using SomeBoundedContext.Application.SomeBusiness1_Area;
using SomeBoundedContext.Contracts.Commands;

namespace SomeBoundedContext.Infrastructure.NServiceBus.SomeBusiness1_Area
{
    /// <summary>
    /// Entry point to the domain.
    /// You can communicate with the domain use only allowed API.
    /// </summary>
    internal sealed class SomeBusiness1AreaBusHandler /* : ICommandHandler Inhereted from something in NuGet Shared lib*/
    {
        // This class has to be stateless and work as singleton.
        // Very easy layer. Without anything. Just for transferring command to domain
        // Only for command

        private readonly SomeBusiness1ApplicationService _applicationService;

        // Use Constructor injection
        public SomeBusiness1AreaBusHandler(SomeBusiness1ApplicationService applicationService)
        {
            _applicationService = applicationService ?? throw new ArgumentNullException(nameof(applicationService));
        }

        public void Handle(SomeCommand command)
        {
            _applicationService.Handle(command);
        }
    }
}