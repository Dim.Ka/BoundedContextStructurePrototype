﻿using System;
using SomeBoundedContext.Infrastructure.NServiceBus.SomeBusiness1_Area;
using StructureMap;

namespace SomeBoundedContext
{
    public sealed class SomeBoundedContextMicroservice /* : Some of MicroserviceBase. Inhereted from something in NuGet Shared lib */
    {
        /* Have to be in base class. Have not to be property. It's just for test container.*/

        public Container Container { get; private set; }

        public SomeBoundedContextMicroservice()
        {
            Container = new Container();
        }

        public void Build()
        {
            Container.Configure(ConfigureContainer());
        }

        public void Stop()
        {
            if (Container != null)
            {
                Container.Dispose();
                Container = null;
            }
        }

        // Class just can fill container necessary dependencies.
        private Action<ConfigurationExpression> ConfigureContainer()
        {
            return _ =>
            {
                _.ForConcreteType<SomeBusiness1AreaBusHandler>().Configure.Singleton();
            };
        }
    }
}