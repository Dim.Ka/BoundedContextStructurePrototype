﻿using System;

namespace SomeBoundedContext.Contracts.Dtos
{
    public sealed class SomeData
    {
        // This class is Unchangeable => It's good.

        public SomeData(int data, Guid id)
        {
            Data = data;
            Id = id;
        }

        public int Data { get; }
        public Guid Id { get; }
    }
}