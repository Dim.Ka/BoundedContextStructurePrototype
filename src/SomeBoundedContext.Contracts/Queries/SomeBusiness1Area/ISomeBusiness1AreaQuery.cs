﻿using System;

namespace SomeBoundedContext.Contracts.Queries.SomeBusiness1Area
{
    public interface ISomeBusiness1AreaQuery
    {
        string[] GetNamesByCity(string city);

        string GetCityByName(string name);

        Guid[] GetSomeBusinessIdsByCity(string city);

        // ... Other Methods. They need to return concret data.
        // Only for UI, reporting somthing else.
        // If other bounded context needs to have some data it means it has to store it in his microservice.
    }
}