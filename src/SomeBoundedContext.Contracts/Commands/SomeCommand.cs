﻿using System;
using SomeBoundedContext.Contracts.Dtos;

namespace SomeBoundedContext.Contracts.Commands
{
    public sealed class SomeCommand /* : Inhereted from something in NuGet Shared lib */
    {
        // This class is Unchangeable => It's good.


        /// <summary>
        /// RequestId is needed for identification Business operation lately when will be some event that this request was handled. After handling you can do something
        /// Data is just some data.
        /// </summary>

        public SomeCommand(int requestId, SomeData data)
        {
            RequestId = requestId;
            Data = data ?? throw new ArgumentNullException(nameof(data));
        }

        public int RequestId { get; }
        public SomeData Data { get; }
    }
}